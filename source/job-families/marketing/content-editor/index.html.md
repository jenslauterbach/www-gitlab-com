---
layout: job_page
title: "Editor"
---

## Content Editor

### Responsibilities

- Write blog posts and marketing copy as needed. 
- Commission and edit posts by team members and external writers.
- Assist in developing news and announcements as requested within Marketing and by other departments or partners.
- Sub-edit the contributions of other team members to ensure a consistent GitLab brand and voice.
- Maintain a high standard of well written and factually accurate content.
- Stay up to date on GitLab's current and upcoming products and features.

### Requirements

- Experience as a content writer, editor, or similar role, preferably in enterprise marketing. 
- Strong communication skills without a fear of over communication. 
- Extremely detail-oriented and organized, able to meet deadlines.
- Familiarity of the software development process including Git, CI and CD
- Experience with writing for SEO and keyword research. 
- You share our [values](/handbook/values), and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission.

## Managing Editor 

### Responsibilities

- Oversee all daily publishing operations, including managing the editorial calendar and maintaining a regular publishing cadence. 
- Report on blog metrics and use data to inform publishing strategy. 
- Improve and enforce GitLab's editorial style guide, including brand style, tone, and voice. 
- Plan and manage GitLab's quartlerly Content Hack Day. 
- Sub-edit blog contributions from internal and external writers.  
- Writing and reviewing for external publications as needed. 
- Editing for corporate communications as needed. 
- Drive improvements to user experience on the GitLab blog. 
- Maintain a high standard of well written and factually accurate content.
- Maintain a deep understanding of GitLab's current and upcoming products and features.

### Requirements 

- Degree in English, journalism or media, or equivalent work experience. 
- Proven experience as a managing editor or similar role, preferably within enterprise marketing. 
- Proven ability to copywrite and edit.
- Proven experience with SEO and keyword research. 
- Excellent communication, organizational, and leadership capabilities. 
- Markdown proficiency. 
- Familiarity of the software development process including Git, CI and CD
- You share our [values](/handbook/values), and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission. 
