---
title: "GitLab's Functional Group Updates - May 16th - May 26th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: company
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Product Team

[Presentation slides](https://www.slideshare.net/JobvanderVoort/gitlab-product-update-may-16)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/TdoGXMxdejg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Backend Team

[Presentation slides](https://docs.google.com/presentation/d/1kWZxBMROnyHG5yx7ag_Ub3gOttw3EwVAM4W8iEnteoE/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/hqogQzvufhU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### General Team

[Presentation slides](https://docs.google.com/presentation/d/1qq1Pnz9991WhGT6lm6mgytbua5q5Gv3g8Vm7bW83T4o/edit#slide=id.g1c74eaa825_1_0)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/sHoShguNKdg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### People Ops Team

[Presentation slides](https://docs.google.com/presentation/d/1BpNIktAAsLNuN5NibEsw554z1ICfh8GRct4MdUZoVXE/edit#slide=id.g153a2ed090_0_63)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/qK5g33yt6E4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
